package taker

import (
	"bytes"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"

	mgo "gopkg.in/mgo.v2"

	"bitbucket.com/csc_auto_sdch_proxy/cutil"
	"bitbucket.com/csc_auto_sdch_proxy/cutil/cerrors"
	"bitbucket.com/csc_auto_sdch_proxy/cutil/storage"
	"bitbucket.com/csc_auto_sdch_proxy/sdch"
)

type Context struct {
	InitialMgoSession *mgo.Session
	cutil.Logger
	*Config
	Storage storage.Storage
}


func (ctx *Context) ServeOrErr(w http.ResponseWriter, r *http.Request) (err error) {
	ctx.Info("Got request")
	logRequest(ctx, r)

	if err = checkRequest(r); err != nil {
		return err
	}
	ctx.Debug("Request check done")

	var body []byte
	if body, err = readBody(r); err != nil {
		return err
	}
	ctx.Debug("Body read done")

	key := hashKey(body)
	if err = ctx.Storage.Write(key, body); err != nil {
		return cerrors.CustomInternal{"storage write", err}
	}
	ctx.Debug("Body write to Storage done. Key:", key)

	var page *sdch.Page
	if page, err = getPageInfo(r.Header, key, len(body)); err != nil {
		return err
	}
	ctx.Debug("Get page info done")

	if err = mgoPageInfo(ctx, page); err != nil {
		return err
	}
	ctx.Debug("Mgo query done")

	w.WriteHeader(http.StatusCreated)
	ctx.Debug("Response done")
	return nil
}

func mgoPageInfo(ctx *Context, page *sdch.Page) error {
	mgoSession := ctx.InitialMgoSession.Clone()
	defer mgoSession.Close()
	pageCol := mgoSession.DB(ctx.Database).C(ctx.PageCollection)
	err := pageCol.Insert(page)
	if err != nil {
		return cerrors.CustomInternal{"mongoDB insert error", err}
	}
	return nil
}

func getPageInfo(h http.Header, key string, bodySize int) (*sdch.Page, error) {
	const (
		HostHeader            = "X-Fwd-Request-Host"
		URIHeader             = "X-Fwd-Request-URI"
		RefererHeader         = "X-Fwd-Request-Referer"
		ContentTypeHeader     = "Content-Type"
		ContentEncodingHeader = "Content-Encoding"
	)
	mandatoryHeaders := []string{HostHeader, URIHeader}
	for _, header := range mandatoryHeaders {
		if h.Get(header) == "" {
			return nil, cerrors.CustomClient{fmt.Sprintf("missing header: %s", header), nil}
		}
	}

	return &sdch.Page{
		Host:            h.Get(HostHeader),
		Key:             key,
		URI:             h.Get(RefererHeader),
		Referer:         h.Get(RefererHeader),
		ContentType:     h.Get(ContentTypeHeader),
		ContentEncoding: h.Get(ContentEncodingHeader),
		ContentLength:   int32(bodySize),
	}, nil

}

func hashKey(data []byte) (key string) {
	hash := sha512.Sum512(data)
	return hex.EncodeToString(hash[:])
}

func checkRequest(r *http.Request) error {
	if r.Method != "POST" {
		return cerrors.UnsupportedMethod{r.Method}
	}
	return nil
}

func readBody(r *http.Request) ([]byte, error) {
	var buff bytes.Buffer
	if _, err := buff.ReadFrom(r.Body); err != nil {
		return nil, cerrors.CustomInternal{"request body read error", err}
	}
	return buff.Bytes(), nil
}

func logRequest(log cutil.Logger, r *http.Request) {
	if data, err := json.Marshal(
		struct {
			Method string
			URL    string
			Header http.Header
		}{
			r.Method,
			r.URL.String(),
			r.Header,
		},
	); err == nil {
		log.Debugf("Request: %s", data)
	} else {
		log.Errf("Request serialization err: %s", err)
	}
}
