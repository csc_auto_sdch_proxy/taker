package main

import (
	"errors"
	"log"

	"github.com/cocaine/cocaine-framework-go/cocaine"
	mgo "gopkg.in/mgo.v2"

	"bitbucket.com/csc_auto_sdch_proxy/cutil/cerrors"
	"bitbucket.com/csc_auto_sdch_proxy/cutil/storage"
	"bitbucket.com/csc_auto_sdch_proxy/taker"
)

func getContextAndLoop(logger *cocaine.Logger) (err error) {
	var config *taker.Config
	if config, err = taker.LoadConfig(); err != nil {
		return cerrors.CustomInternal{"confil load error", err}
	}
	logger.Info("Got config")

	logger.Debug("Try get storage")
	var st storage.Storage
	if st, err = storage.New(config.StorageNamespace, storage.DefaultEndpoint); err != nil {
		return cerrors.CustomInternal{"get storadge service error", err}
	}
	logger.Info("Got storadge")

	logger.Debug("Try connect to mongo")
	var mgoSession *mgo.Session
	if mgoSession, err = mgo.Dial(config.MongoURI); err != nil {
		return cerrors.CustomInternal{"mongoDB dial error", err}
	}
	defer mgoSession.Close()
	logger.Info("Connected to MongoDB")

	//set up session
	logger.Debug("Setting up mongo")
	mgoSession.SetSafe(&mgo.Safe{}) //check for errors
	mgoSession.SetMode(mgo.Monotonic, true)

	binds := map[string]cocaine.EventHandler{
		"take": cocaine.WrapHandler(
			cerrors.HTTPHandler{
				&taker.Context{
					mgoSession,
					logger,
					config,
					st,
				},
			},
			logger,
		),
	}
	logger.Debug("Creating Worker")
	var worker *cocaine.Worker
	if worker, err = cocaine.NewWorker(); err != nil {
		return cerrors.CustomInternal{"new worker error", err}
	}
	logger.Info("Worker created")
	worker.Loop(binds)
	return
}

func main() {
	var err error
	var logger *cocaine.Logger
	if logger, err = cocaine.NewLogger(); err != nil {
		log.Fatalf("Can't get logger: %s", err)
	}
	logger.Info("Started")

	err = getContextAndLoop(logger)
	if err == nil {
		err = errors.New("must not sucsessful return from worker loop")
	}
	logger.Err(err)
	log.Fatal(err)
}
