package taker

import (
	"encoding/json"
	"os"
)

type Config struct {
	MongoURI         string `json:"mongoURI"`
	Database         string `json:"database"`
	PageCollection   string `json:"pageCollection"`
	HostCollection   string `json:"hostCollection"`
	StorageNamespace string `json:"storageNamespace"`
}

const configFileName = "config.json"

func LoadConfig() (config *Config, err error) {
	var file *os.File
	if file, err = os.Open(configFileName); err == nil {
		config = new(Config)
		err = json.NewDecoder(file).Decode(&config)
	}
	return
}
